import Controller from '@ember/controller';
export default Controller.extend({
  actions: {
    async signUp(event) {
      event.preventDefault();
      // await this.model.save();
      // Ember.Logger.debug(this);
      await this.transitionToRoute('login');
    }
  }
});

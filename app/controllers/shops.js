import Controller from '@ember/controller';
import { empty } from '@ember/object/computed';
export default Controller.extend({
  isAddingShop: false,
  newShopName:'',
  isEditingShop: false,
  isAddButtonDisabled: empty('newShopName'),
  actions: {
    addShop() {
      this.set('isAddingShop', true);
    },
    cancelAddShop() {
      this.set('isAddingShop', false);
    },
    editShop(){
      this.toggleProperty('isEditingShop');
    },
    cancelEditShop() {
      this.set('isEditingShop', false);
    },
    async editSubmit(newShopName){
      event.preventDefault();
      this.item.set('name', newShopName);
      this.setProperties({
        newShopName: '',
        isEditingShop: false
      });
    },
    async saveShop(event) {
      event.preventDefault();
      let newShop = this.store.createRecord('shop', { name: this.newShopName });
      await newShop.save();
      this.setProperties({
        newShopName: '',
        isAddingShop: false
      });
      this.transitionToRoute('shops.shop.products', newShop.id);
    },
  }
});

import Controller from '@ember/controller';
import { empty } from '@ember/object/computed';
export default Controller.extend({
  isAddingProduct: false,
  newProductName:'',
  newProductPrice:0,
  newProductQuantity:0,
  isAddButtonDisabled: empty('newProductName'),
  actions: {
    addProduct() {
      this.set('isAddingProduct', true);
    },
    cancelAddProduct() {
      this.set('isAddingProduct', false);
    },
    async saveProduct(event) {
      event.preventDefault();
      let newProduct = this.get('store').createRecord('product', {
        name: this.get('newProductName'),
        price: this.get('newProductPrice'),
        quantity: this.get('newProductQuantity'),
        shop: this.model
      });
      await newProduct.save();
      this.set('newProductName', '');
      this.set('newProductQuantity', 0);
      this.set('newProductPrice', 0);
    }
  }
});

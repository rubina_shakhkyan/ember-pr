import DS from 'ember-data';
const { Model, attr, belongsTo } = DS;
export default Model.extend({
 name: attr('string'),
 price: attr('number'),
 quantity: attr('number'),
 shop: belongsTo(),
});

import DS from 'ember-data';
const { Model, attr, hasMany } = DS;

export default Model.extend({
  name: attr('string'),
  products: hasMany('products'),
  init() {
    this._super(...arguments);
      let count = 0;
      this.products.forEach(function(p){
        count+= p.price*p.quantity;
      });
      this.set('sum', count);
    },
});

import Component from '@ember/component';

export default Component.extend({
  isEditingProduct: false,
  productName:'',
  productPrice:0,
  productQuantity:0,
  actions: {
    editProduct() {
      this.set('isEditingProduct', true);
    },
    async editSubmit(event) {
      event.preventDefault();
      let item = this.model;
      item.setProperties({'name':this.get('productName'), 'price': this.get('productPrice'), 'quantity': this.get('productQuantity')});
      await item.save();
      this.set('isEditingProduct', false);
    }
  }

});

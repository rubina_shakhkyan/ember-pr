import Component from '@ember/component';

export default Component.extend({
  actions: {
    async deleteShop(shop){
       shop.destroyRecord();
    }
  }
});

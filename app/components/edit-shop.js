import Component from '@ember/component';

export default Component.extend({
  isEditingShop: false,
  shopName: '',
  actions: {
    editShop() {
      this.set('isEditingShop', true);
    },
    async editSubmit(event) {
      event.preventDefault();
      let item = this.model;
      item.set('name',this.get('shopName'));
      await item.save();
      this.set('isEditingShop', false);
    },
  }
});

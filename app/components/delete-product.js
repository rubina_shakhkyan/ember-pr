import Component from '@ember/component';

export default Component.extend({
  actions: {
    async deleteProduct(product){
       product.destroyRecord();
    }
  }
});
